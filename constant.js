module.exports = {
    invalidUser: {
        message: "The user couldn't find",
        errMsg: "INVALID_USER"
    },
    invalidMail: {
        message: "Please enter valid e-mail address!",
        errMsg: "INVALID_MAIL"
    },
    notFoundUser: {
        message: "User does not exist!",
        errMsg: "NOT_FOUND_USER"
    },
    invalidName: {
        message: "Please enter valid name (At least 3-character)",
        errMsg: "INVALID_USER_NAME"
    },
    invalidPassword: {
        message: "Please enter valid Password (At least 6-character)",
        errMsg: "INVALID_PASSWORD"
    },
    createdUser: {
        message: "The User successfully created!",
        successMsg: "CREATED_USER"
    },
    hashError: {
        message: "The password couldn't hash(I will shown for debug mode, not for production)!",
        errMsg: "HASH_ERROR"
    },
    userExist: {
        message: "The user already exist!",
        errMsg: "EXIST_USER"
    },
    successLogin: {
        message: "Successfully Logged in!",
        successMsg: "SUCCESS_LOGIN"
    },
    failLogin: {
        message: "Wrong password or e-mail address!",
        errMsg: "LOGIN_FAILED"
    },
    successLogOut: {
        message: "Successfully logged out!",
        successMsg: "SUCCESS_LOG_OUT"
    },
    notLoggedIn: {
        message: "You are not logged in!",
        errMsg: "NOT_LOGGED_IN"
    },
    updatedPassword: {
        message: "Your password Updated! (This password is temporary) Please update password after you log in!",
        successMsg: "UPDATED_PASSWORD"
    },
    unAuthorizedUser: {
        message: "Unauthorized User!",
        errMsg: "UNAUTH_USER"
    },
    invalidToken: {
        message: "Invalid User Token!",
        errMsg: "INVALID_TOKEN"
    },
    notSuppliedToken: {
        message: "Auth token is not supplied!",
        errMsg: "NOT_SUPPLIED_TOKEN"
    },
    failedGetData: {
        message: "Couldn't get the related data!",
        errMsg: "FAIL_GET_DATA"
    },
    emptyToken: {
        message: "Couldn't find any token!",
        errMsg: "NOT_FOUND_TOKEN"
    },
    emptyAmount: {
        message: "Couldn't find amount value!",
        errMsg: "NOT_FOUND_AMOUNT"
    },
    invalidAmount: {
        message: "Amount value type is invalid!",
        errMsg: "INVALID_AMOUNT_VALUE"
    },
    notFoundToken: {
        message: "Requested token couldn't be found!",
        errMsg: "NOT_FOUND_TOKEN"
    },
    invalidExchange: {
        message: "Exchange values cannot be same!",
        errMsg: "INVALID_EXCHANGE"
    },
    insufficientAmount: {
        message: "You do not have enough balance!",
        errMsg: "INSUFFICIENT_AMOUNT"
    }
}