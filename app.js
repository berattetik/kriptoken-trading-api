const express = require('express'),
    cookieParser = require('cookie-parser'),
    cors = require('cors'),
    errorhandler = require('errorhandler'),
    mongoose = require('mongoose'),
    registrationRoutes = require('./api/routes/registration'),
    tradingRoutes = require('./api/routes/trading');

require('dotenv').config();

var isProduction = process.env.NODE_ENV === 'production';

// Create global app object
var app = express();

app.use(cors());
app.use(cookieParser())
// Normal express config defaults
app.use(require('morgan')('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

if (!isProduction) {
    app.use(errorhandler());
}

if (isProduction) {
    mongoose
        .connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
        .then(() => console.log("Successfully connected to DB"))
        .catch(err => console.log("Unable to connect", err));
} else {
    mongoose
        .connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
        .then(() => console.log("Successfully connected to DB"))
        .catch(err => console.log("Unable to connect", err));
    mongoose.set('debug', true);
    mongoose.set('useFindAndModify', false);
}

require('./models/User');
require('./models/Currency');


app.use(`/api/${process.env.API_VERSION}/`, registrationRoutes)
app.use(`/api/${process.env.API_VERSION}/trading`, tradingRoutes)


/// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
if (!isProduction) {
    app.use(function (err, req, res, next) {
        console.log(err.stack);
        res.status(err.status || 500);

        res.json({
            'errors': {
                message: err.message,
                error: err
            }
        });
    });
}

// production error handler
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        'errors': {
            message: err.message,
            error: {}
        }
    });
});

module.exports = app;