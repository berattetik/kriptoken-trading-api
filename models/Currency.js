var mongoose = require('mongoose');

var CurrencySchema = new mongoose.Schema({
    name: {
        type: String
    },
    value: {
        type: Number
    },
    baseType: {
        type: String
    }
})

module.exports = mongoose.model('Currency', CurrencySchema, "currencies");