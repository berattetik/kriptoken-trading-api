const mongoose = require('mongoose');
// const uniqueValidator = require('mongoose-unique-validator');
const jwt = require('jsonwebtoken');
const secret = require('../config').secret;

const balanceSchema = new mongoose.Schema({
    XRP: {
        type: mongoose.Types.Decimal128,
        default: 10000,
        min: 0
    },
    ETH: {
        type: mongoose.Types.Decimal128,
        default: 0,
        min: 0
    },
    LTC: {
        type: mongoose.Types.Decimal128,
        default: 0,
        min: 0
    },
    DASH: {
        type: mongoose.Types.Decimal128,
        default: 0,
        min: 0
    }
}, { _id: 0, usePushEach: true, })

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        lowercase: true,
        unique: true,
        required: [true, "can't be blank"],
        match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
        // index: true
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: [true, "can't be blank"],
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        // index: true
    },
    password: {
        type: String,
        required: [true, "can't be blank"]
    },
    balance: {
        XRP: {
            type: mongoose.Types.Decimal128,
            default: 10000
        },
        ETH: {
            type: mongoose.Types.Decimal128,
            default: 0
        },
        LTC: {
            type: mongoose.Types.Decimal128,
            default: 0
        },
        DASH: {
            type: mongoose.Types.Decimal128,
            default: 0
        }
    }
}, { timestamps: true, usePushEach: true });

UserSchema.methods.generateJWT = function () {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign({
        id: this._id,
        username: this.username,
        email: this.email,
        exp: parseInt(exp.getTime() / 1000),
    }, process.env.SECRET);
};

UserSchema.methods.toAuthJSON = function () {
    return {
        username: this.username,
        email: this.email,
        token: this.generateJWT()
    };
};

UserSchema.statics.isUserExist = function (email) {
    return new Promise((resolve, reject) => {
        this.findOne({ email: email })
            .then(user => {
                resolve((user ? user : false))
            })
            .catch(err => {
                if (process.env.NODE_ENV !== 'production')
                    reject(false);
            });
    })
};

module.exports = mongoose.model('User', UserSchema, 'User');
