//Models
const User = require('../../models/User');
const Currency = require('../../models/Currency');
//validator
const { isEmpty } = require('validator');
const { getCurrentKriptoValues } = require('../helper/helper');
//response resources
const constant = require('../../constant.js');
const { getTokenFromHeader } = require('../middleware/checkAuth');


exports.exchange = function (req, res, next) {
    const { exchange } = req.params;
    const { amount, buy } = req.query;
    let sellCurrency, buyCurrency, netBTCValue;
    if (!exchange || isEmpty(exchange))
        return res.status(404).json(constant.emptyToken)
    if (!buy || isEmpty(buy))
        return res.status(404).json(constant.emptyToken)
    if (isEmpty(amount))
        return res.status(404).json(constant.emptyAmount)
    if (isNaN(amount))
        return res.status(404).json(constant.invalidAmount)

    Currency
        .find({}, { _id: 0 })
        .then((currencies) => {
            sellCurrency = currencies.filter(currency => currency.name == exchange.toUpperCase());
            buyCurrency = currencies.filter(currency => currency.name == buy.toUpperCase());

            //if selected and buy exchange are same
            if (buy.toUpperCase() === sellCurrency[0].name)
                return res.status(403).json(constant.invalidExchange)

            //if requested Currency does not exist on this Trading Platform
            if (!sellCurrency || !sellCurrency.length)
                return res.status(404).json(constant.notFoundToken)

            return User.findById(req.decoded.id, { balance: 1, _id: 0 })
        })
        .then(result => {
            const { balance } = result;
            //net User balance worth as BTC
            netBTCValue = parseFloat(balance[sellCurrency[0].name]) * sellCurrency[0].value;

            //request to buy with given amount by user
            requestedBTCValue = buyCurrency[0].value * amount;

            //if the user doesn't have enough token in order to buy selected token
            if (netBTCValue < requestedBTCValue)
                return res.status(200).json(constant.insufficientAmount)

            let remainedSoldTokenValue = (netBTCValue - requestedBTCValue) / sellCurrency[0].value

            return User
                .findByIdAndUpdate(
                    req.decoded.id,
                    {
                        $set: {
                            [`balance.${sellCurrency[0].name}`]: remainedSoldTokenValue,
                            [`balance.${buy.toUpperCase()}`]: parseFloat(balance[buyCurrency[0].name]) + parseFloat(amount)
                        }
                    }, { 'new': true })
        })
        .then((updatedValue) => {
            return res.status(200).json(updatedValue.balance)
        })
        .catch(err => {
            return res.status(404).json(constant.failedGetData)
        });
}

exports.getAllKriptoValues = async function (req, res, next) {
    const currencies = await getCurrentKriptoValues();

    User
        .findById(req.decoded.id, { _id: 0, createdAt: 0, updatedAt: 0 })
        .then((user) => {
            if (!user) return res.status(404).json(constant.notFoundUser)
            return res.status(200).json({ currencies, user })
        }).catch((err) => {
            return res.status(404).json(constant.failedGetData)
        });
}
