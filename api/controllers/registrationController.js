//Models
const User = require('../../models/User');
//validator
const { isEmail, normalizeEmail, isEmpty } = require('validator');
//response resources
const constant = require('../../constant.js');
//encryption & generate token packages
const bcrypt = require('bcrypt');
const generator = require('generate-password');


exports.signUp = function (req, res, next) {
    const { username, email, password } = req.body;
    if (!req.body.username || typeof req.body.username !== "string" || isEmpty(username) || username.length < 3)
        return res.status(422).json(constant.invalidName)

    if (!email || typeof email !== "string" || !isEmail(normalizeEmail(email)))
        return res.status(422).json(constant.invalidMail)

    if (!password || typeof password !== "string" || isEmpty(password) || password.length < 6)
        return res.status(422).json(constant.invalidPassword)

    User.isUserExist(email).then((result) => {
        if (result) {
            return res.status(422).json(constant.userExist)
        }
        //hash the user password
        bcrypt
            .hash(password, parseInt(process.env.SALT), function (err, hashedPassword) {
                if (err)
                    return res.status(404).json(hashError)

                //creating new user
                const user = new User({
                    username,
                    email,
                    password: hashedPassword
                })

                //Update the DB
                return user.save()
                    .then(function (user) {
                        return res.status(201).json(constant.createdUser)
                    })
                    .catch(function (err) {
                        return res.status(404).json({ err })
                    });
            })

    }).catch(err => {
        return res.status(422).json({ err: "Something went wrong" })
    });
}

exports.login = function (req, res, next) {
    const { email, password } = req.body;
    if (!email || typeof email !== "string" || !isEmail(normalizeEmail(email)))
        return res.status(422).json(constant.invalidMail)

    if (!password || typeof password !== "string" || isEmpty(password) || password.length < 6)
        return res.status(422).json(constant.invalidPassword)

    User
        .isUserExist(email)
        .then((user) => {
            if (!user)
                return res.status(200).json(constant.notFoundUser);

            //compare user password
            bcrypt
                .compare(password, user.password)
                .then(compareResult => {
                    if (compareResult) {
                        const token = user.generateJWT()

                        return res.cookie("token", token, { maxAge: 60 * 60 * 1000, path: '/home' }).header("x-auth-token", token).status(200).json({ ...constant.successLogin, token })
                    } else {
                        return res.status(401).json(constant.failLogin)
                    }
                })
        })
        .catch(err => {
            return res.status(404).json({ errMsg: "Something went wrong", error: err })
        });
}

exports.logout = (req, res, next) => {
    return res.clearCookie("token", { path: '/' }).status(200).json(constant.successLogOut);

}

exports.forgotPassword = (req, res, next) => {
    const { email } = req.body;
    if (isEmpty(email) || !isEmail(email)) return res.status(500).json(constant.invalidMail);

    User
        .isUserExist(email)
        .then((user) => {
            if (!user)
                return res.status(200).json(constant.notFoundUser);

            const password = generator.generate({
                length: 8,
                numbers: true
            });

            bcrypt
                .hash(password, parseInt(process.env.SALT), function (err, hashedPassword) {
                    if (err)
                        return res.status(404).json(hashError)

                    user.password = hashedPassword;
                    // //Update the DB
                    return user
                        .save()
                        .then(function () {
                            return res.status(201).json({ ...constant.updatedPassword, "newPassword": password })
                        })
                        .catch(function (err) {
                            return res.status(404).json({ err })
                        });
                })
        })
}

