const jwt = require('jsonwebtoken');
//response resources
const constant = require('../../constant.js');

exports.getTokenFromHeader = (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }
    return null;
}

exports.getTokenFromCookie = (req, res, next) => {
    return req.cookies["token"] && req.cookies["token"]
}

exports.checkAuth = (req, res, next) => {
    const token = this.getTokenFromHeader(req) || this.getTokenFromCookie(req)
    if (token) {
        jwt.verify(token, process.env.SECRET, (err, decoded) => {
            if (err)
                return res.status(422).json(constant.invalidToken)
            req.decoded = decoded
            next();
        });
    }
    else {
        return res.status(401).json(constant.notSuppliedToken)
    }
}
