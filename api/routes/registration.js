var router = require('express').Router();
var { login, signUp, forgotPassword, logout } = require('../controllers/registrationController')

router.post('/login', login);
router.post('/logout', logout);
router.post('/register', signUp);
router.post('/forgotpassword', forgotPassword);

module.exports = router;