const router = require('express').Router(),
    { checkAuth } = require('../middleware/checkAuth'),
    { exchange, getAllKriptoValues } = require('../controllers/tradingController')

router.get('/getAllExchange', checkAuth, getAllKriptoValues);

router.post('/:exchange', checkAuth, exchange);

module.exports = router;